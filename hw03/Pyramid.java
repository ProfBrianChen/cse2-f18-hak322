//Author: Hayatullah Khan
//hw03: Pyramid.java

import java.util.Scanner;   //used when you want the user's input
//main method required for every java program
public class Pyramid{
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );  //Scanner construction
    System.out.print ("Enter the square side of the pyramid: ");  //asks the user to input the side length of square base
    double baseLength = myScanner.nextDouble();   //reads the user's square length input
    System.out.print ("Enter the vertical height of the pyramid: ");  //asks user to input the pyramid's height
    double verticalHeight = myScanner.nextDouble();   //reads the user's height input 
    double pyramidVolume = ((baseLength*baseLength)*verticalHeight)/3 ;   //calculates the pyramid's volume 
    System.out.print ("The volume of the pyramid with the given dimensions is: " +pyramidVolume);   //prints the pyramid's volume
  } //end of main method 
}  //end of class