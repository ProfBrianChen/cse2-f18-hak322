 //Author: Hayatullah Khan
//hw03: Convert

import java.util.Scanner;   //used when you want the user's input
//main method required for every java program
public class Convert{
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );  //Scanner construction
    System.out.print ("Enter the affected area in acres: ");
    double affectedArea = myScanner.nextDouble();  //asks user to input the size of the affected area in acres
    System.out.print ("Enter the rainfall in inches: ");
    double rainfall = myScanner.nextDouble();  //asks user to input rainfall in inches
    double rainfallInMiles = rainfall*0.0000157828;  //converts rainfall in inches to miles
    double acresToMiles = (affectedArea/640);  //converts acres to miles
    System.out.print ("The rainfall in cubic miles: " + (acresToMiles*rainfallInMiles));
  }
}