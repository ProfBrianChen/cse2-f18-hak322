//////////Hayatullah Khan
/////6/9/18 
////Bicycle cyclometer
//Document your program. What does MPG do? Place comments here!
public class Cyclometer {
  public static void main (String[] args) {
    int secsTrip1=480;    ////Time taken in seconds to complete Trip 1
    int secsTrip2=3220;   ////Time taken in seconds to complete Trip 2
    int countsTrip1=1561;    ////Number of rotations of front wheel for Trip 1 
    int countsTrip2=9037;    ////Number of rotations of front wheel for Trip 2
    double wheelDiameter=27.0,   ////double is used for accuracy. It does not get rid of decimal places
    PI=3.14159, ///value of Pi
    feetPerMile=5280,
    inchesPerFoot=12,
    secondsPerMinute=60;
    
    double distanceTrip1, distanceTrip2, totalDistance;    ////
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute) +" minutes and had "+ countsTrip1+" counts.");
    
    System.out.println("Trip 2 took "+
                       (secsTrip2/secondsPerMinute)+"minutes and had "+
                       countsTrip2+" counts.");
    //
    //
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //above gives distance in inches 
    //(for each count, a rotation of the wheel travles
    //the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; //gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
   //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
  }
    
}