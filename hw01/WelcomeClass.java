public class WelcomeClass{
  public static void main(String args []){
    System.out.println("    -----------\n" + "    | WELCOME |\n" + "    -----------\n" + "  ^  ^  ^  ^  ^  ^\n" + " / \\/ \\/ \\/ \\/ \\/ \\\n" + "<-H--A--K--3--2--2->\n" + " \\ /\\ /\\ /\\ /\\ /\\ /\n" + "  v  v  v  v  v  v\n" + "My name is Hayat Khan. The middle of three\n" + "children, I was born in Pakistan. I am \n" + "currently a first-year student at Lehigh \n" + " with an undecided major, but I do tend to \n" + "shift towards the STEM side of academics\n" + "so I will most probably major in that area.");
  }
}
  