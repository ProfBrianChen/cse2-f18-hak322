//////Author: Hayatullah Khan
//////Arithmetic Caculations

///Main method required for every java program
public class Arithmetic{
  public static void main (String[] args) {
      // Pants
      int numPants = 3;    // Number of pants
      double pantsPrice = 34.98;    // Cost per unit of pants
      // Sweatshirts
      int numShirts = 2;   // Number of sweatshirts
      double shirtPrice = 24.99;    // Cost per unit of shirts
      // Belts
      int numBelts = 1;   // Number of belts
      double beltPrice = 33.99;   // Cost per unit of belts
      // PA sales tax
      double paSalesTax = 0.06;   // Sales tax in PA
    
      double totalCostOfPants = numPants*pantsPrice;   //Total cost of Pants without PA tax
      double totalCostOfSweatshirts = numShirts*shirtPrice;   //Total cost of Sweatshirts without PA tax
      double totalCostOfBelts = numBelts*beltPrice;    //Total cost of Belts without PA tax
      
      double salesTaxOnSweatShirts = 0.06*totalCostOfSweatshirts;  //Total tax on all Sweatshirts with many decimal places
      salesTaxOnSweatShirts = (int)(salesTaxOnSweatShirts * 100);  //Conversting total tax on all Sweatshirts to integer form
      salesTaxOnSweatShirts = salesTaxOnSweatShirts / 100;         //Total tax on all Sweatshirts with 2 decimal places
    
      double salesTaxOnPants = 0.06*totalCostOfPants;  //Total tax on all Pants with many decimal places
      salesTaxOnPants = (int)(salesTaxOnPants * 100);  //Conversting total tax on all Pants to integer form
      salesTaxOnPants = salesTaxOnPants / 100;         //Total tax on all Pants with 2 decimal places
    
      double salesTaxOnBelts = 0.06*totalCostOfBelts;  //Total tax on all Belts with many decimal places
      salesTaxOnBelts = (int)(salesTaxOnBelts * 100);  //Conversting total tax on all Belts to integer form
      salesTaxOnBelts = salesTaxOnBelts / 100;         //Total tax on all Belts with 2 decimal places
      
      double totalCostWithNoTax = totalCostOfPants+totalCostOfSweatshirts+totalCostOfBelts;//Total cost of purchase of all items excluding tax
      
      totalCostWithNoTax = (int)(totalCostWithNoTax * 100);
      totalCostWithNoTax = totalCostWithNoTax /100;
      
      double totalSalesTax = salesTaxOnPants+salesTaxOnSweatShirts+salesTaxOnBelts;     //Total sales tax on all of the items
      
      double totalCostWithTax = totalCostWithNoTax+totalSalesTax;   //Total cost of entire purchse with PA sales tax
      
      System.out.println("The cost of each pair of pants is " +pantsPrice);   //Prints the cost of a pair of pants
      System.out.println("The cost of each sweatshirt is " +shirtPrice);    //Prints the cost of a sweatshirts
      System.out.println("The cost of each belt is " +beltPrice);    //Prints the cost of a belt
    
      System.out.println("The total sales tax on all of the pants is " +salesTaxOnPants);   //Prints the total sales tax on all of the pants
      System.out.println("The total sales tax on all of the sweathsirts is " + salesTaxOnSweatShirts);   //Prints the total sales tax on all of the sweatshirts
      System.out.println("The total sales tax on all of the belts is " +salesTaxOnBelts);   //Prints the total sales tax on all of the belts
      
      System.out.println("The total cost of buying all the pants without sales tax is " +totalCostOfPants);   //Prints the total cost of pants without sales tax
      System.out.println("The total cost of buying all the sweatshirts without sales tax is " +totalCostOfSweatshirts);    //Prints the total cost of sweathsirts without sales tax
      System.out.println("The total cost of buying all the belts without sales tax is " +totalCostOfBelts);   ////Prints the total cost of belts without sales tax
      
      System.out.println("The total cost of the entire purchase is without sales tax is " +totalCostWithNoTax);   //Prints the total cost of the entire purchase without sales tax
      System.out.println("The total sales tax of of the entire purchase is " +totalSalesTax);   //Prints the total sales tax on the entire purchase
      System.out.println("The total cost of the entire purchase with sales tax is " +totalCostWithTax);   //Prints the total cost of the entire purchase with sales tax  
    
 
  }
}