//Written by: Hayat Khan
//hw04: CrapsIf
//This program involves the rolling of two six sided dice and evaluating the result of the roll of the dice
import java.util.Scanner;
import java.util.Random;
public class CrapsIf{   //main class
  public static void main (String [] args){    //main method

    
      Random rI = new Random();   //To generate a random variable
      Scanner cI = new Scanner(System.in);   //To define a scannner
      int r1;  //To define roll 1
      int r2;  //To define roll 2
      int option;  //To define roll 3
      System.out.println("Which way do you like? 1: Random numbers 2: Enter your own numbers");
  
        option = cI.nextInt();  //To scan the user's input
        if(option  == 1){   
            r1 = rI.nextInt(6) + 1;
            r2 = rI.nextInt(6) + 1; 
        }
        else {
          System.out.println("Enter the first number");
          r1 = cI.nextInt();
          System.out.println("Enter the second number");
          r2 = cI.nextInt();
        } 
  
    
    ///Combination 1: When first dice roll number comes out 1
    if(r1 == 1){
      if(r2 == 1){System.out.println("Snake eyes");}
      if(r2 == 2){System.out.println("Ace Deuce");}
      if(r2 == 3){System.out.println("Easy Four");}
      if(r2 == 4){System.out.println("Fever Five");}
      if(r2 == 5){System.out.println("Easy Six");}
      if(r2 == 6){System.out.println("Seven Out");}
      }
    ///Combination 2: When first dice roll number comes out 2
    if(r1 == 2){
      if(r2 == 2){System.out.println("Hard Four");}
      if(r2 == 3){System.out.println("Fever Five");}
      if(r2 == 4){System.out.println("Easy Six");}
      if(r2 == 5){System.out.println("Seven Out");}
      if(r2 == 6){System.out.println("Easy Eight");}
    }
    ///Combination 3: When first dice roll number comes out 3
    if(r1 == 3){
      if(r2 == 3){System.out.println("Hard Six");}
      if(r2 == 4){System.out.println("Seven Out");}
      if(r2 == 5){System.out.println("Easy Eight");}
      if(r2 == 6){System.out.println("Nine");}
    }
    ///Combination 4: When first dice roll number comes out 4
    if(r1 == 4){
      if(r2 == 4){System.out.println("Hard Eight");}
      if(r2 == 5){System.out.println("Nine");}
      if(r2 == 6){System.out.println("Easy Ten");}
    }
    ///Combination 5: When first dice roll number comes out 5
    if(r1 == 5){
      if(r2 == 5){System.out.println("Hard Ten");}
      if(r2 == 6){System.out.println("Yo-leven");}
    }
    ///Combination 6: When first dice roll number comes out 6
    if(r1 == 6){
      if(r2 == 6){System.out.println("BoxCars");}
    }
    
}}
  