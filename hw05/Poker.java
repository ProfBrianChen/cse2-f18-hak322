//Author: Hayatullah Khan
//Project: GIves the probability of certain hands in poker


import java.util.InputMismatchException;   
import java.util.Random;
import java.util.Scanner;

public class Poker {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		boolean notInt = false;
		int input = 0;
		while(!notInt) {  //enforces integer input
			try {
				input = in.nextInt();
				notInt = true;
			}
			catch(InputMismatchException e) {  //catches non integer input
				System.out.println("Please enter an integer");
				in.nextLine();
			}
		}
		
		Random r = new Random();  //generates random numbers
		int fourOfKind = 0, threeOfKind = 0, twoPairs = 0, onePair = 0, fullHouse = 0;
		int[] hand = new int[5];  //saves a hand of five cards
		boolean[] cards = new boolean[52];
		int[] rank = new int[13];
		int card = 0;
		for (int i = 0; i < input; i++) {
			for (int j = 0; j < hand.length; j++) {  //generates a hand of five cards 
				boolean validCard = false;
				while(!validCard) {  //ensures duplicate cards are not used
					card= r.nextInt(52);
					if(!cards[card]) {
						cards[card] = true;
						validCard = true;
					}
				}

		//		hand[j] = card%13;  //finds the rank of a card 
				rank[card%13] = rank[card%13] + 1;  //finds rank 
			}
			for (int j = 0; j < rank.length; j++) {
				if(rank[j] == 4) {
					fourOfKind++;

				}
					
				if(rank[j] == 3) {
					boolean isFullHouse = false;
					for (int j2 = j+1; j2 < rank.length; j2++) {
						if(rank[j2] == 2) {
							fullHouse++;
							isFullHouse = true;
							break;
						}
					}
					if(isFullHouse)
						break;
					for (int j2 = j-1; j2 >= 0; j2--) {
						if(rank[j2] == 2) {
							fullHouse++;
							isFullHouse = true;
							break;
						}
					}
					if(!isFullHouse)
						threeOfKind++;
				}
				if(rank[j] == 2) {
					boolean isTwoPair = false;
					for (int j2 = j+1; j2 < rank.length; j2++) {
						if(rank[j2] == 2) {
							twoPairs++;
							isTwoPair = true;
							break;
						}
					}
					if(!isTwoPair)
						onePair++;
				}
			}
			for (int j = 0; j < cards.length; j++) {
				cards[j] = false;
				
			}
			for (int j = 0; j < rank.length; j++) {
				rank[j] = 0;
			}
			
		}
		System.out.println("number of iterations are " +input);
		System.out.println("probability of Four-of-kind = " + (double) fourOfKind/input);
		System.out.println("probability of Three-of-kind = " + (double) threeOfKind/input);
		System.out.println("probability of full-House = " + (double) fullHouse/input);
		System.out.println("probability of Two-pairs = " + (double) twoPairs/input);
		System.out.println("probability of One-pair = " + (double) onePair/input);
	}

}
